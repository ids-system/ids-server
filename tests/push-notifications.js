
const PushNotifications = require('@pusher/push-notifications-server');
const config = require('../config');

let pushNotifications = new PushNotifications(config.pusher);

pushNotifications.publish(['hello'], {
  apns: {
    aps: {
      alert: 'Hello!'
    }
  },
  fcm: {
    data: {
      "isMyPushNotification": true
    }
  },

}).then((publishResponse) => {
  console.log('Just published:', publishResponse.publishId);
  return true;
}).catch((error) => {
  console.log('Error:', error);
});

