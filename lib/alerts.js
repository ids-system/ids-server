const PushNotifications = require("@pusher/push-notifications-server");
const config = require("../config");

var pushNotifications = new PushNotifications(config.pusher);

var Alerts={};

Alerts.deviceLowBatt = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                notification: {
                    title: "Device low batt",
                    body: "The device is running on low battery"
                },
                data: {
                    reason: "DEVICE_LOW_BATT",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("DEVICE_LOW_BATT Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
            return false;
        });
};

Alerts.deviceWakeUp = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                notification: {
                    title: "Device UP",
                    body: "The device has woken up"
                },
                data: {
                    reason: "DEVICE_WOKE_UP",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("DEVICE_WOKE_UP Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
            return false;
        });
};

Alerts.gatewayWakeUp = function() {
    pushNotifications
        .publish(["alert"], {
            notification: {
                title: "Gateway UP",
                body: "The gateway has woken up"
            },
            fcm: {
                data: {
                    reason: "GATEWAY_WOKE_UP",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("GATEWAY_WOKE_UP Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
        });
};

Alerts.deviceSeemsDead = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                notification: {
                    title: "Device DOWN",
                    body: "The device seems down"
                },
                data: {
                    reason: "DEVICE_SEEMS_DEAD",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("DEVICE_SEEMS_DEAD Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
        });
};

Alerts.gatewaySeemsDead = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                notification: {
                    title: "Gateway DOWN",
                    body: "The gateway seems down"
                },
                data: {
                    reason: "GATEWAY_SEEMS_DEAD",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("GATEWAY_SEEMS_DEAD Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
        });
};

Alerts.sendAlert = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                data: {
                    isMyPushNotification: true,
                    reason: "ALARM",
                    ts: Date()
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("ALARM Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
        });
};

Alerts.stopAlert = function() {
    pushNotifications
        .publish(["alert"], {
            fcm: {
                notification: {
                    title: "Alert ACK",
                    body: "Alert was ack'd"
                },
                android: {
                    priority: "high"
                }
            }
        })
        .then(publishResponse => {
            console.log("ACK Alert published:", publishResponse.publishId);
            return true;
        })
        .catch(error => {
            console.log("Error:", error);
        });
}

module.exports = Alerts;