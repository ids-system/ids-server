var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
var concat = require('concat-stream');
var chalk =require('chalk');

var alertsRouter = require('./routes/alerts');

var app = express();

var morganMiddleware = logger(function (tokens, req, res) {
  var statusCode = tokens.status(req, res);
  return [
      chalk.red.bold('🚨 '),
      chalk.white(new Date().toISOString()),
      chalk.white.bold(tokens.method(req, res)),

      chalk.white(tokens.url(req, res)),

      statusCode==200?  chalk.green.bold(statusCode) :
      statusCode==500?  chalk.red.bold(statusCode) :
                        chalk.yellow.bold(statusCode),

      chalk.white("("+tokens['response-time'](req, res) + 'ms'+")"),
    ].join(' ');
});

var rawBodyParser = function (req, res, next) {
  req.pipe(concat(function(data){
    req.rawBody = data;
    next();
  }));
};

var notFoundHandler = function(req, res, next) {
  next(createError(404));
};

app.use(morganMiddleware);
app.use(rawBodyParser);
app.use(alertsRouter);
app.use(notFoundHandler);

//err
//app.use(function(err, req, res, next) {
//  res.status(err.status || 500);
//  res.json(err);
//});

module.exports = app;
