const express = require("express");
const alerts = require("../lib/alerts");
const Parser = require("binary-parser").Parser;

var fs = require('fs');
var csvWriter = require('csv-write-stream')

var writer = csvWriter({
  separator: ';',
  newline: '\n',
  headers: [
    "received",
    "timerGateway",
    "timerDevice",
    "rssi",
    "deviceBattVoltage",
    "deviceBattPercent",
    "deviceLowBatt",
    "devicePirState",
    "deviceAlarmActive"
  ],
  sendHeaders: true
})
writer.pipe(fs.createWriteStream('state.csv', {flags: 'a'}));


var router = express.Router();

var state = {
  alertIsRunning: false,
  deviceSeemsDead: false,
  gatewaySeemsDead: false,
  lastSeenGateway: null,
  lastSeenDevice: null,
  lastAlert: null,
  lastAck: null
};

var lastDeviceAliveTimeout = null;
var lastGatewayAliveTimeout = null;

function resetDeviceDeadTimeout(){
  clearTimeout(lastDeviceAliveTimeout);
  lastDeviceAliveTimeout = setTimeout(function(){
    console.error("> Device seems dead !");
    state.deviceSeemsDead = true;
    alerts.deviceSeemsDead();
  }, 2 * 60 * 1000);
}

function resetGatewayDeadTimeout(){
  clearTimeout(lastGatewayAliveTimeout);
  lastGatewayAliveTimeout = setTimeout(function(){
    console.error("> Gateway seems dead !");
    state.gatewaySeemsDead = true;
    alerts.gatewaySeemsDead();
  }, 2 * 60 * 1000);
}

//status request
router.get("/status", function(req, res, next) {
  console.info("status");
  res.status(200).json(state);
});

//ack alert request
router.get("/ack", function(req, res, next) {
  console.info("ack request");
  //TODO: recup %batterie
  console.log(req.body);
  state.lastAck = new Date();
  if (state.alertIsRunning) {
    console.info("> Alert stopped !");
    state.alertIsRunning = false;
    alerts.stopAlert();
  }
  res.send("ok")
});

router.get("/csv", function(req, res, next) {
  console.info("csv export");
  res.download('./state.csv','state.csv');
});


router.post("/device/state", function(req, res, next) {

  console.info("device state request");

  var deviceState = new Parser()
    .endianess("little")
    .uint8("rssi")
    .array("timerGateway", {
      type: "uint8",
      length: 8
    })
    .array("timerDevice", {
      type: "uint8",
      length: 8
    })
    .float("deviceBattVoltage")
    .uint8("deviceBattPercent")
    .uint8("devicePirState")
    .uint8("deviceAlarmActive")
    .uint8("deviceLowBatt");

  // Prepare buffer to parse.
  var buf = Buffer.from(req.rawBody);
  console.log(buf.length);

  var deviceStateObj=deviceState.parse(buf);
  console.log(deviceStateObj);

  //TODO: some checksums/validate data

  state.lastSeenGateway = new Date();
  state.lastSeenDevice = new Date();

  if(deviceStateObj.deviceAlarmActive){
    state.lastAlert = new Date();
    if (!state.alertIsRunning) {
      console.error("> Intrusion detected !");
      state.alertIsRunning = true;
      alerts.sendAlert();
    }
  }

  if (state.gatewaySeemsDead) {
    console.info("> Gateway woken up !");
    state.gatewaySeemsDead = false;
    alerts.gatewayWakeUp();
  }

  if (state.deviceSeemsDead) {
    console.info("> Device woken up !");
    state.deviceSeemsDead = false;
    alerts.deviceWakeUp();
  }

  resetGatewayDeadTimeout();
  resetDeviceDeadTimeout();

  res.send("ok");
  deviceStateObj.received= Date.now();
  writer.write(deviceStateObj);
});


//ping request
router.get("/device/alive", function(req, res, next) {
  console.info("device alive request");
  //TODO: recup %batterie
  console.log(req.body);
  state.lastSeenDevice = new Date();
  if (state.deviceSeemsDead) {
    console.info("> Device woken up !");
    state.deviceSeemsDead = false;
    alerts.deviceWakeUp();
  }

  resetGatewayDeadTimeout();
  resetDeviceDeadTimeout();
  res.send("ok");
});

router.get("/device/lowbatt", function(req, res, next) {
  console.info("device low batt request");
  alerts.deviceLowBatt();
  res.send("ok");
});

//gateway ping request
router.get("/gateway/alive", function(req, res, next) {
  console.info("gateway alive request");
  state.lastSeenGateway = new Date();
  if (state.gatewaySeemsDead) {
    console.info("> Gateway woken up !");
    state.gatewaySeemsDead = false;
    alerts.gatewayWakeUp();
  }
  resetGatewayDeadTimeout();
  res.send("ok");
});

//alert request
router.get("/alert", function(req, res, next) {
  console.info("alert request");
  state.lastAlert = new Date();
  if (!state.alertIsRunning) {
    console.error("> Intrusion detected !");
    state.alertIsRunning = true;
    alerts.sendAlert();
  }
  resetGatewayDeadTimeout();
  resetDeviceDeadTimeout();
  res.send("ok");
});

module.exports = router;
